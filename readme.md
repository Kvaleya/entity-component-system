# Entity component system C# framework

This documentation does not cover the basic concepts of 
entity-component-system programming style, but details this particular 
implementation.

Note that while this ECS implementation works, its performance is suboptimal.
I am not sure why exactly, but I suspect boxing may be happening when accessing
struct components while simulating entities.

## Introduction

This framework (and ECS in general) uses three basic building blocks of game 
logic:

- Entity - an object in the game world, the behaviour of which is defined by 
what components it contains
- Component - some data assigned to entities and used processors
- Processor - contains game logic, iterates upon a set of all entities that 
contain specific components

The user only needs to provide the framework with a list of processors and 
call the *Tick* method, everything else happens automatically. All entities 
and components are managed by the framework. Each tick processors are 
executed in the correct order of dependency and in multiple threads (using C# 
tasks) - a processor can iterate upon its entities in multiple threads and 
multiple processors may be executed in parallel, if the dependency graph 
allows such execution. The framework also provides means to easily pass 
singleton data to processors and to check the dependency graph for potencial 
multithreaded component access problems.

## Using the framework

First, an instance of *EcsManager* must be created. This will hold all 
entities, components and processors (often also called systems in the context 
of ECS) and is used to execute all major operations. Most imporant is the 
*Tick* method, which executes all processors and also does some internal 
actions.

Processors can be added or removed using the *EcsManager.AddProcessors* and 
*RemoveProcessors* method. Multiple processors can be added/removed using a 
single call, which should be the preferred way of adding processors, 
especially at runtime, since calling these methods will cause the internal 
processor dependency graph to be rebuilt.

## Entities

All entity actions are deferred until the next tick. This is done to allow 
processors, potencially running in parallel, to add, remove or modify 
entities at will without interupting other processors. These deferred actions 
are executed at the beginning of each tick.

Entities can be added or removed using *AddEntity* and *RemoveEntity*. 
AddEntity takes the list of the new entity's components as arguments and 
returns the entity object. This entity will not be assigned to processors or 
be paired with its components until the next tick.

Entities can be modified using *EntityAddComponents*, which takes the new 
component objects as arguments, or using *EntityRemoveComponents*, which 
takes the types of components that are to be removed as arguments. Both 
actions are also deferred until the next tick.

## Components

Any class or struct can be used as a component. However, it is highly advised 
that all components that hold any data should be structs. A component that 
holds no data at all is possible and useful, common use case is when a 
processor is meant to be executed only upon a subset of entities that have 
the required (data) components. These entities can be "marked" with a 
component of a type specific to this system.

## Processors

All processors must inherit the *EntityProcessor* class and should contain a 
*Process* method, which will be used to process a single entity. Its first 
argument must be an Entity, the other arguments should be the components this 
processor requires, all passed as a reference (using the ref keyword). When 
the processor is executed, this method will be called for each entity and the 
correct components will be provided automatically.

Also, the component arguments of the Process method define what entities this 
processor will be executed on: it will be all entities that contain at least 
the specified components.

During execution, the processor can modify the components passed as 
references to the Process method. Unlike entity modifications, changes to 
components are not deferred and processors executed after the current one 
will see them.

Processors can also "break the rules" and access entities or components other 
than what they are executed upon using *GetExternalComponent* and 
*SetExternalComponent* methods or test for the existence of components inside 
an entity using *EntityHasComponent* and *TryGetExternalComponent* methods.

There are also virtual methods available, such as *OnEntityAdded*, which is 
called when a new entity is added to the list of entities processed by this 
processors (be it from being newly created or gaining the necessary 
components), similarly *OnEntityRemoved*. *OnInit* is called after the 
processor is added to the EcsManager object, *OnDestroy* when it is removed, 
*OnProcessingStart* is called each tick just before entities are processed by 
this processor, similarly *OnProcessingEnd*

Processor behaviour can be further customized using special class attributes. 
*ProcessorExecution* specifies whether the processor should be executed 
sequentially, not at all (in this case the Process method is not required), 
or in parallel (where multiple threads iterate upon this processor's entities 
and Process method), and minimal number of items the processor must have to 
use parallel execution.

*ProcessorExecuteAfter* and *ProcessorExecuteBefore* specify the order in 
which the processors should be executed. These attributes are what creates 
edges in processor dependency graph.

*ProcessorComponentUsage* gives hint to the EcsManager about what components 
this processor actually interacts with. The components specified in the 
Process method are taken into account automatically, so this concerns 
component access using *GetExternalComponent* type methods. Process method 
arguments are considered to be read-write, unless they are marked with the 
*ComponentReadOnly* attribute.

Different processors may also be executed in parallel if the dependency graph 
allows it. However care must be taken to properly set the dependencies, 
otherwise two different processors may attempt to interact with the same set 
of components at the same time.

## Processor DAG

When adding new processor, the dependency graph is rebuild. An exception is 
thrown when a cycle occurs in this graph.

Each processor maintains a debug set of component types it interacts with, 
which is filled from the Process method arguments, ProcessorComponentUsage 
attribute and also automatically from GetExternalComponent and similar 
methods. This set can be used to check for potencial multithreading problems 
using the *EscManager.CheckComponentAccess* method. Any two processors which 
may write into a single component type in parallel will be reported.

## Singleton components

Sometimes processors need to access global data that is not entity-bound, 
such as the time delta for the current tick. Singleton components solve this 
by providing a mechanism to easily pass an object to all processors that 
request it. Processor can request access to a singleton component by creating 
a property (with a setter) of the required type marked with the 
SingletonComponent attribute. *EcsManager.SetSingletonProperty* can be used 
to assign a singleton component. It takes an object as an argument, the 
singleton component, and assigns it to all processors that have the marked 
property of this object's type.

## Component storage

The heart of this ECS implementation is the ComponentStorage class. For each 
component type, one instance of this class is created. It contains two lists, 
one that holds the component data, other that stores the owner entity ID for 
each component.

These lists are always ordered by ascending entity ID. This is done in hope 
to assist CPU caches. When a processor iterates upon a set of entites that 
use this component, it will also use ascending ID order, so it should always 
access components that lie ahead in memory in relation to already processed 
components.

Also, when a processor needs to fetch a next component from storage, it will 
first linearly search ahead of the last used component, since it is likely to 
lie in proximity. If that fails, it will fetch the actual index of the next 
component from a dictionary. See the *ComponentStorage.FindFast* method.

Adding a entity-component pair simly adds the ID and the component to the end 
of the lists.

Removing a component will simly mark the owner entity ID of that component as 
invalid by setting its most significant bit. This way, "bubbles" will form in 
the lists. These are removed by calling the *Defrag* method every tick. This 
method keeps track of the leftmost bubble, and upon every call it will move 
this bubble a set number of indices to the right, merging it with other 
bubbles it encounters along the way. This way, all invalid entries will end 
up at the right edge of the lists eventually, making the lists practically 
bubble-free.

## Adding components to an existing entity

When adding a new component to an existing entity, one cannot just add the 
new component into its type's storage, because the ascending order of entity 
IDs in storage may be broken if another entity with a higher ID already 
exists and uses this component type.

Instead the entity with a new component is assigned a new ID, and all old 
components are moved to this new ID in their storage.

This does not happen when removing components, since there is no danger of 
breaking the ascending order.

## Possible future features:

- Enable/disable processors: thus eliminating the need to reconstruct the 
dependency graph if a processor needs to be disabled for a period of time
Sparse processors: only run on 1/N of entities each tick, a common 
optimization in many games
- Complete state copy: useful for multiplayer/network games

## Demo

The demo consists of a simple particle system done using ECS and OpenGL. All 
game logic code in the demo is located within the "Logic" folder. The game 
loop uses OpenTK's Gamewindow loop and is located mostly within 
Window.OnRenderFrame.
