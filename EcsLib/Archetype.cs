﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcsLib
{
	// Entity archetype class
	// The archetype of an entity is defined by the types of components it uses (order independent)
	class Archetype : IEquatable<Archetype>
	{
		readonly Type[] _componentTypes;

		public int Length { get { return _componentTypes.Length; } }

		public Type this[int index]
		{
			get { return _componentTypes[index]; }
		}

		// Component types should not contain duplicates
		public Archetype(params Type[] componentTypes)
		{
			if(componentTypes == null)
				throw new ArgumentNullException(nameof(componentTypes));

			// Order types by name to make archetypes easier to compare and order independent
			_componentTypes = componentTypes.OrderBy(type => type.FullName).ToArray();
		}

		public static bool SystemEntityCompatible(Archetype system, Archetype entity)
		{
			int j = 0;

			for(int i = 0; i < system._componentTypes.Length; i++)
			{
				var type = system[i];

				while(entity[j] != type)
				{
					j++;

					if(j == entity.Length)
						return false;
				}
			}

			return true;
		}

		// Generate a hash of a list based on its contents
		// Adapted from https://stackoverflow.com/questions/7278136/create-hash-value-on-a-list
		public static int GetSequenceHashCode<T>(IList<T> sequence)
		{
			const int seed = 487;
			const int modifier = 31;

			unchecked
			{
				return sequence.Aggregate(seed, (current, item) =>
					(current * modifier) + item.GetHashCode());
			}
		}

		public bool Equals(Archetype other)
		{
			if(ReferenceEquals(null, other)) return false;
			if(ReferenceEquals(this, other)) return true;

			// Compare type array contents
			if(_componentTypes.Length != other._componentTypes.Length)
				return false;

			for(int i = 0; i < _componentTypes.Length; i++)
			{
				if(_componentTypes[i] != other._componentTypes[i])
					return false;
			}

			return true;
		}

		public override bool Equals(object obj)
		{
			if(ReferenceEquals(null, obj)) return false;
			if(ReferenceEquals(this, obj)) return true;
			if(obj.GetType() != this.GetType()) return false;
			return Equals((Archetype)obj);
		}

		public override int GetHashCode()
		{
			return (_componentTypes != null ? GetSequenceHashCode(_componentTypes) : 0);
		}

		public static bool operator ==(Archetype left, Archetype right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(Archetype left, Archetype right)
		{
			return !Equals(left, right);
		}
	}
}
