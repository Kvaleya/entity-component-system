﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EcsLib
{
	internal class ObjectPool<T>
		where T : new()
	{
		List<T> _storage = new List<T>();
		int _validEntries = 0;

		public int Count { get { return Math.Max(0, _validEntries); } }

		/// <summary>
		/// Can be called from multiple threads
		/// </summary>
		/// <returns></returns>
		public T Get()
		{
			int entry = Interlocked.Decrement(ref _validEntries);
			if(entry >= 0)
			{
				return _storage[entry];
			}
			else
			{
				return new T();
			}
		}

		/// <summary>
		/// Must be called only when no other thread is using the EntityPool
		/// </summary>
		/// <param name="e"></param>
		public void Return(T e)
		{
			if(_validEntries < 0)
			{
				_validEntries = 0;
			}

			if(_validEntries < _storage.Count)
			{
				_storage[_validEntries] = e;
			}
			else
			{
				_storage.Add(e);
			}

			_validEntries++;
		}
	}
}
