﻿using System;

namespace EcsLib
{
	[Flags]
	public enum ComponentUsageType
	{
		Read = 1,
		Write = 2,
		ReadWrite = 3,
	}

	public enum ProcessorExecutionType
	{
		/// <summary>
		/// Ignore the system's Process method completely
		/// </summary>
		DoNotExecure,
		/// <summary>
		/// Sequential execution, no parallelism
		/// </summary>
		Sequential,
		///// <summary>
		///// A separate task is created for each entity. Higher per-entity overhead than PerThreadLoop. Best used when the time it takes to process entities can vary significantly.
		///// </summary>
		//PerEntityTask,
		/// <summary>
		/// Several threads are launched, each thread is assigned a range of entities that it will process using a for-loop. Best used when the time to process each entity is relatively constant, otherwise one of the threads might run significantly longer than others, reducing the performance gain from parallelism. Use PerEntityTask in that case.
		/// </summary>
		PerThreadLoop,
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public class ProcessorComponentUsageAttribute : Attribute
	{
		internal ComponentUsageType UsageType;
		internal Type[] ComponentTypes;

		public ProcessorComponentUsageAttribute(ComponentUsageType usageType, params Type[] componentTypes)
		{
			UsageType = usageType;
			ComponentTypes = componentTypes;
		}
	}

	[AttributeUsage(AttributeTargets.Property)]
	public class SingletonComponentAttribute : Attribute
	{
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public class ProcessorExecuteBeforeAttribute : Attribute
	{
		internal Type[] Processors;

		public ProcessorExecuteBeforeAttribute(params Type[] processors)
		{
			Processors = processors;
		}
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = true)]
	public class ProcessorExecuteAfterAttribute : Attribute
	{
		internal Type[] Processors;

		public ProcessorExecuteAfterAttribute(params Type[] processors)
		{
			Processors = processors;
		}
	}

	[AttributeUsage(AttributeTargets.Class, Inherited = false, AllowMultiple = false)]
	public class ProcessorExecutionAttribute : Attribute
	{
		internal ProcessorExecutionType ProcessorExecutionType;
		internal int MinItems;

		public ProcessorExecutionAttribute(ProcessorExecutionType type, int minItemsForParallel = 500)
		{
			ProcessorExecutionType = type;
			MinItems = minItemsForParallel;
		}
	}

	[AttributeUsage(AttributeTargets.Parameter, Inherited = false, AllowMultiple = false)]
	public class ComponentReadOnlyAttribute : Attribute
	{
		public ComponentReadOnlyAttribute()
		{
		}
	}
}
