﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace EcsLib
{
	public abstract class EntityProcessor : IIndexStorage
	{
		const string ProcessMethodName = "Process";

		protected EcsManager Manager;

		internal HashSet<Type> ComponentsRead = new HashSet<Type>();
		internal HashSet<Type> ComponentsWrite = new HashSet<Type>();
		internal List<Type> ExecuteAfter = new List<Type>();
		internal List<Type> ExecuteBefore = new List<Type>();
		internal List<Type> ComponentsRequired = new List<Type>();
		internal ProcessorExecutionType ExecutionType = ProcessorExecutionType.PerThreadLoop;
		internal int MinParallelItems = 0;
		internal Action<Entity, int[]> ExecuteMethod;
		internal MethodInfo ProcessMethodInfo;
		internal Archetype Archetype;
		internal int TopologicalOrder;
		Dictionary<Type, PropertyInfo> _singletonComponentProperties = new Dictionary<Type, PropertyInfo>();
		ComponentStorage<Entity> _entities; // Bend ComponentStorage to store entities
		internal Dictionary<ulong, int> _entityIdToIndex = new Dictionary<ulong, int>();

		public bool AllowExecution = true;

		public int ValidCount { get { return _entities.ValidCount; } }
		public int ContainerSize { get { return _entities.ContainerSize; } }

		public IEnumerable<Entity> AllEntities()
		{
			return _entities.AllEntries();
		}

		void IIndexStorage.OnIndexChanged(ulong owner, Type componentType, int newIndex)
		{
			_entityIdToIndex[owner] = newIndex;
		}

		internal void SetManager(EcsManager manager)
		{
			Manager = manager;
			_entities = new ComponentStorage<Entity>(manager, this);
		}

		internal void TrySetSingletonComponentProperty(object component)
		{
			if(!_singletonComponentProperties.ContainsKey(component.GetType()))
				return;
			var property = _singletonComponentProperties[component.GetType()];
			property.SetValue(this, component);
		}

		/// <summary>
		/// Use reflection to gather processor attributes
		/// </summary>
		internal void GetAttributes()
		{
			var type = this.GetType();
			var attributes = type.GetCustomAttributes(false);

			foreach(var attribute in attributes)
			{
				var usage = attribute as ProcessorComponentUsageAttribute;
				var before = attribute as ProcessorExecuteBeforeAttribute;
				var after = attribute as ProcessorExecuteAfterAttribute;
				var parallel = attribute as ProcessorExecutionAttribute;

				if(usage != null)
				{
					if(usage.UsageType.HasFlag(ComponentUsageType.Read))
					{
						foreach(var componentType in usage.ComponentTypes)
						{
							ComponentsRead.Add(componentType);
						}
					}

					if(usage.UsageType.HasFlag(ComponentUsageType.Write))
					{
						foreach(var componentType in usage.ComponentTypes)
						{
							ComponentsWrite.Add(componentType);
						}
					}
				}

				if(before != null)
				{
					ExecuteBefore.AddRange(before.Processors);
				}

				if(after != null)
				{
					ExecuteAfter.AddRange(after.Processors);
				}

				if(parallel != null)
				{
					ExecutionType = parallel.ProcessorExecutionType;
					MinParallelItems = parallel.MinItems;
				}
			}

			foreach(var property in type.GetProperties(BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public))
			{
				if(property.GetCustomAttribute<SingletonComponentAttribute>() != null)
				{
					if(property.SetMethod == null)
					{
						throw new Exception("A processor's singleton component properties must have a setter!");
					}

					_singletonComponentProperties.Add(property.PropertyType, property);
				}
			}

			if(ExecutionType != ProcessorExecutionType.DoNotExecure)
			{
				ProcessMethodInfo = type.GetMethod(ProcessMethodName, BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public);

				if(ProcessMethodInfo == null)
				{
					throw new Exception("The processor does not have a Process method (Type: " + type.FullName + ")");
				}

				var parameters = ProcessMethodInfo.GetParameters();

				for(int i = 0; i < parameters.Length; i++)
				{
					var p = parameters[i];

					if(i == 0)
					{
						if(p.ParameterType != typeof(Entity))
						{
							throw new Exception("The processor does not have a valiud Process method, the first argument (Entity) is missing. (Type: " + type.FullName + ")");
						}
					}
					else
					{
						var componentType = p.ParameterType.GetElementType();

						ComponentsRequired.Add(componentType);
						ComponentsRead.Add(componentType);

						if(p.GetCustomAttribute<ComponentReadOnlyAttribute>() == null)
						{
							ComponentsWrite.Add(componentType);
						}
					}
				}
			}

			Archetype = new Archetype(ComponentsRequired.ToArray());
		}

		/// <summary>
		/// Execute the processor on a specified range of its entities. The index and count are used to access the processor's internal entity storage, which may contain invalid entries. When executing the processor sequentially, the given count parameter may be larger than the actual entity count because of this.
		/// </summary>
		/// <param name="firstIndex"></param>
		/// <param name="count"></param>
		internal void Execute(int firstIndex, int count)
		{
			int lastIndex = firstIndex + count;
			int entIndex = firstIndex;

			// The index into each component type's storage
			int[] components = new int[ComponentsRequired.Count];
			ComponentStorage[] storage = new ComponentStorage[ComponentsRequired.Count];

			for(int i = 0; i < storage.Length; i++)
			{
				storage[i] = Manager.GetStorage(ComponentsRequired[i], false);
			}

			// This is the important loop
			while(entIndex < lastIndex && entIndex < _entities.ContainerSize)
			{
				if(_entities.IsValid(entIndex))
				{
					//if(components == null)
					//{
					//	// Inicialize the indices using binary search
					//	components = new int[ComponentsRequired.Count];

					//	for(int i = 0; i < components.Length; i++)
					//	{
					//		components[i] = storage[i].Find(_entities.GetOwner(entIndex));
					//	}
					//}
					//else
					{
						// Advance the indices using forward only search
						for(int i = 0; i < components.Length; i++)
						{
							components[i] = storage[i].FindFast(_entities.GetOwner(entIndex), components[i]);
						}
					}

					ExecuteMethod.Invoke(_entities[entIndex], components);
				}

				entIndex++;
			}
		}

		internal void Defrag()
		{
			_entities.Defrag();
		}

		protected bool EntityHasComponent<T>(Entity e)
		{
			return e.ComponentIndices.ContainsKey(typeof(T));
		}

		protected bool TryGetExternalComponent<T>(Entity e, out T item)
			where T : struct
		{
			if(Manager.Debug)
				ComponentsRead.Add(typeof(T));

			if(!e.ComponentIndices.ContainsKey(typeof(T)))
			{
				item = new T();
				return false;
			}

			int index = e.ComponentIndices[typeof(T)];
			var storage = Manager.GetStorage<T>(false);
			item = storage[index];
			return true;
		}

		/// <summary>
		/// Try to get a component of any entity
		/// </summary>
		/// <typeparam name="T"></typeparam>
		/// <param name="e"></param>
		/// <returns></returns>
		protected T GetExternalComponent<T>(Entity e)
		{
			if(Manager.Debug)
				ComponentsRead.Add(typeof(T));

			int index = e.ComponentIndices[typeof(T)];
			var storage = Manager.GetStorage<T>(false);
			return storage[index];
		}

		protected void SetExternalComponent<T>(Entity e, T value)
		{
			if(Manager.Debug)
				ComponentsWrite.Add(typeof(T));

			int index = e.ComponentIndices[typeof(T)];
			var storage = Manager.GetStorage<T>(false);
			storage[index] = value;
		}

		internal void AddEnt(Entity e)
		{
			int index = _entities.Add(e.ID, (object)e);
			_entityIdToIndex.Add(e.ID, index);
			OnEntityAdded(e);
		}

		internal void ChangeEntId(ulong oldId, Entity e)
		{
			_entities.Remove(_entityIdToIndex[oldId]);
			int index = _entities.Add(e.ID, (object)e);
			_entityIdToIndex.Remove(oldId);
			_entityIdToIndex[e.ID] = index;
		}

		internal void RemoveEnt(Entity e)
		{
			_entities.Remove(_entityIdToIndex[e.ID]);
			_entityIdToIndex.Remove(e.ID);
			OnEntityRemoved(e);
		}

		internal void OnProcessingStartInternal(int numThreads)
		{
			OnProcessingStart(numThreads);
		}

		internal void OnProcessingEndInternal()
		{
			OnProcessingEnd();
		}

		internal void OnInitInternal()
		{
			OnInit();
		}

		internal void OnDestroyInternal()
		{
			OnDestroy();
		}

		/// <summary>
		/// Called just before entity processing.
		/// </summary>
		/// <param name="numThreads">NumThreads can be 1 for sequential processing, >1 for multithreaded processing or 0 when the ProcessorExecutionType is set to DoNotExecure</param>
		protected virtual void OnProcessingStart(int numThreads)
		{

		}

		protected virtual void OnProcessingEnd()
		{

		}

		protected virtual void OnEntityAdded(Entity e)
		{

		}

		protected virtual void OnEntityRemoved(Entity e)
		{

		}

		protected virtual void OnInit()
		{
			
		}

		protected virtual void OnDestroy()
		{
			
		}
	}
}
