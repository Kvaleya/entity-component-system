﻿using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Text;

namespace EcsLib
{
	interface IIndexStorage
	{
		void OnIndexChanged(ulong owner, Type componentType, int newIndex);
	}

	// Using abstract class instead of an interface, with the hope to make things slightly faster
	internal abstract class ComponentStorage
	{
		public abstract int Add(ulong owner, object component);
		public abstract object Get(int index);
		public abstract void Remove(int componentIndex);
		//public abstract int FindBinarySearch(ulong owner);
		public abstract int Find(ulong owner);
		public abstract int FindFast(ulong owner, int startingIndex);
		public abstract void Defrag();
		public abstract string PrintContents();
	}

	class ComponentStorage<T> : ComponentStorage
	{
		const ulong InvalidBit = 0x8000000000000000ul;
		//internal int OneDefragSize = 1000;
		const int LinearSearchSize = 128;

		const string RemovedComponentException = "Attempting to access a removed component!";
		const string NotFoundException = "There is no component belonging to this entity ID!";

		public T this[int index]
		{
			get { return Data[index]; }
			set { Data[index] = value; }
		}

		public int ValidCount { get { return _validCount; } }
		public int ContainerSize { get { return _owners.Count; } }

		EcsManager _manager;
		IIndexStorage _indexStorage;

		int _validCount = 0;
		internal List<T> Data { get; } = new List<T>();
		int _addIndex = 0;
		List<ulong> _owners = new List<ulong>();

		// Defragmentation:
		// When an item is removed, it is only marked as removed in the _owners list (by id -1), but is not actually removed
		// Thus, there can be empty "bubbles" of removed elements still present in the list
		// These variables keep track of the leftmost (lowest index) bubble index and size
		// When defragmentation is called, elements at the right edge of the bubble are moved to the left edge
		// moving the bubble one index to the right (position is incremented, size is not)
		// If the right edge of the bubble is also a removed element, it is added to the bubble (size is incremented, position is not)
		// Once the leftmost bubble is moved to the right edge of the list, the entire list is defragmented and can be shrunk
		int _firstBubble = 0;
		int _firstBubbleSize = 0;

		public ComponentStorage(EcsManager manager, IIndexStorage indexStorage = null)
		{
			_manager = manager;
			_indexStorage = indexStorage;

			if(indexStorage == null)
			{
				_indexStorage = manager;
			}
		}

		internal IEnumerable<T> AllEntries()
		{
			int index = 0;

			while(index < ContainerSize)
			{
				if(IsOwnerValid(index))
				{
					yield return Data[index];
				}

				index++;
			}
		}

		public override string PrintContents()
		{
			StringBuilder sb = new StringBuilder();

			for(int i = 0; i < Data.Count; i++)
			{
				if(IsOwnerValid(i))
				{
					sb.AppendLine(i.ToString() + ": " + Data[i].ToString());
				}
				else
				{
					sb.AppendLine(i.ToString() + ": " + "(invalid)");
				}
			}

			return sb.ToString();
		}

		public override object Get(int index)
		{
			return this[index];
		}

		public ulong GetOwner(int index)
		{
			return _owners[index];
		}

		public override int Add(ulong owner, object component)
		{
			return Add(owner, (T) component);
		}

		ulong GetOwnerID(int index)
		{
			return _owners[index] & (~InvalidBit);
		}

		public bool IsValid(int index)
		{
			return IsOwnerValid(index);
		}

		bool IsOwnerValid(int index)
		{
			return (_owners[index] & InvalidBit) == 0;
		}

		void SetOwnerInvalid(int index)
		{
			_owners[index] |= InvalidBit;
		}

		/*
		public override int FindBinarySearch(ulong owner)
		{
			return BinSearch(owner, 0, _owners.Count - 1);
		}

		/// <summary>
		/// Both left and right indices are inclusive
		/// </summary>
		/// <param name="owner"></param>
		/// <param name="left"></param>
		/// <param name="right"></param>
		/// <returns></returns>
		int BinSearch(ulong owner, int left, int right)
		{
			while(right - left > 1)
			{
				int mid = (right + left) / 2;

				var midOnwer = GetOwnerID(mid);

				if(midOnwer == owner)
				{
					if(IsOwnerValid(mid))
					{
						return mid;
					}
					else
					{
						throw new Exception(RemovedComponentException);
					}
				}
				if(midOnwer > owner)
				{
					right = mid - 1;
				}
				else
				{
					left = mid + 1;
				}
			}

			for(int i = left; i <= right; i++)
			{
				if(GetOwnerID(i) == owner)
				{
					if(IsOwnerValid(i))
					{
						return i;
					}
					else
					{
						throw new Exception(RemovedComponentException);
					}
				}
			}

			throw new Exception(NotFoundException);
		}
		*/

		public override int Find(ulong owner)
		{
			return _manager.GetEntityById(owner).ComponentIndices[typeof(T)];
		}

		public override int FindFast(ulong owner, int startingIndex)
		{
			if(owner - GetOwnerID(startingIndex) >= LinearSearchSize)
				return Find(owner);

			// Try to find the component linearly if it is nearby
			for(int i = 0; i < LinearSearchSize; i++)
			{
				if(GetOwnerID(startingIndex + i) == owner)
				{
					if(IsOwnerValid(startingIndex + i))
					{
						return startingIndex + i;
					}
					else
					{
						throw new Exception(RemovedComponentException);
					}
				}
			}

			// Otherwise fetch its ID from the dictionary in the entity
			return Find(owner);
		}

		public int Add(ulong ownerEntityId, T item)
		{
			_validCount++;

			int index = _addIndex;

			if(_addIndex == Data.Count)
			{
				
				Data.Add(item);
				_owners.Add(ownerEntityId);
				_addIndex++;
			}
			else
			{
				Data[index] = item;
				_owners[index] = ownerEntityId;
				_addIndex++;
			}
			
			return index;
		}

		public override void Remove(int componentIndex)
		{
			_validCount--;
			SetOwnerInvalid(componentIndex);
			
			if(_firstBubbleSize == 0 || _firstBubble > componentIndex)
			{
				if(componentIndex + 1 == _firstBubble)
				{
					// The removed element is at the "left" edge of the first bubble, thus making it one element larger
					_firstBubbleSize = _firstBubbleSize + 1;
				}
				else
				{
					_firstBubbleSize = 1;
				}

				_firstBubble = componentIndex;
			}
		}

		// See defragmentation comment earlier in the file
		public override void Defrag()
		{
			if(_firstBubbleSize == 0)
				return;

			// Only do a set number of steps per defrag
			// With OneDefragSize=1000, a 1M list will be defragmented in 16.7 seconds at 60 defrag calls per second
			for(int i = 0; i < _manager.DefragSize; i++)
			{
				int rightEdge = _firstBubble + _firstBubbleSize;

				if(rightEdge == _addIndex)
				{
					// The first bubble is located entirely on the right edge of the list - it can be removed
					_addIndex = _firstBubble;
					Data.RemoveRange(_firstBubble, _firstBubbleSize);
					_owners.RemoveRange(_firstBubble, _firstBubbleSize);
					_firstBubble = 0;
					_firstBubbleSize = 0;
					break;
				}

				if((_owners[rightEdge] & InvalidBit) != 0)
				{
					// The right edge is also invalid, this the bubble expands
					_firstBubbleSize++;
				}
				else
				{
					_owners[_firstBubble] = _owners[rightEdge];
					Data[_firstBubble] = Data[rightEdge];

					_owners[rightEdge] |= InvalidBit;

					if(_indexStorage != null)
					{
						// Update the entity with new component location
						_indexStorage.OnIndexChanged(_owners[_firstBubble], typeof(T), _firstBubble);
					}

					_firstBubble++;
				}
			}
		}
	}
}
