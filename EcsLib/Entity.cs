﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EcsLib
{
	public sealed class Entity
	{
		// The ID may change during the entity's lifetime!
		internal ulong ID;

		bool _alive = true;

		public bool Alive
		{
			get { return _alive; }
			internal set { _alive = value; }
		}

		internal Dictionary<Type, int> ComponentIndices = new Dictionary<Type, int>();
		internal List<EntityProcessor> Processors = new List<EntityProcessor>();

		/// <summary>
		/// Note: Do not use constructor to create entities! Use EcsManager.AddEntity instead.
		/// </summary>
		public Entity()
		{
		}

		internal Archetype GetArchetype()
		{
			return new Archetype(ComponentIndices.Keys.ToArray());
		}
	}
}
