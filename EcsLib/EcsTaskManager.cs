﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcsLib
{
	public interface IEcsTaskManager
	{
		void EnqueueProcessorTask(Action a);
		void ParallelForRange(int fromInclusive, int toExclusive, Action<int> onStart, Action<int, int> body);
	}

	class EcsTaskManager : IEcsTaskManager
	{
		public void EnqueueProcessorTask(Action a)
		{
			Task.Factory.StartNew(a);
		}

		/// <summary>
		/// onStart is called before executing the loop, thread count that will be used is passed as the parameter
		/// </summary>
		/// <param name="fromInclusive"></param>
		/// <param name="toExclusive"></param>
		/// <param name="onStart"></param>
		/// <param name="body"></param>
		public void ParallelForRange(int fromInclusive, int toExclusive, Action<int> onStart, Action<int, int> body)
		{
			int threads = Environment.ProcessorCount;

			onStart(threads);

			int range = toExclusive - fromInclusive;
			int stride = range / threads;
			if(range <= 0) threads = 0;
			Parallel.For(0, threads, i =>
			{
				int startInclusive = i * stride;
				int endExclusive = (i == threads - 1) ? toExclusive : startInclusive + stride;
				body(startInclusive, endExclusive);
			});
		}
	}
}
