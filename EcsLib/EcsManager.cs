﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EcsLib
{
	enum EntityActionType
	{
		Create,
		Destroy,
		AddComponents,
		RemoveComponents,
	}

	class DeferredEntityAction
	{
		public EntityActionType Type;
		public Entity Entity;
		public object[] Components;
	}

	class ProcessorGraphNode
	{
		public bool HasValidOrder = false;

		public int Wave = -1;

		public int RemainingDependencies = 0;

		public EntityProcessor Processor;

		public HashSet<ProcessorGraphNode> In = new HashSet<ProcessorGraphNode>();
		public HashSet<ProcessorGraphNode> Out = new HashSet<ProcessorGraphNode>();

		public int Order
		{
			get { return Processor.TopologicalOrder; }
			set { Processor.TopologicalOrder = value; }
		}

		public ProcessorGraphNode(EntityProcessor processor)
		{
			Processor = processor;
		}

		public override string ToString()
		{
			return Processor.GetType().ToString();
		}
	}

	public class EcsManager : IIndexStorage
	{
		public readonly bool Debug = true;

		Dictionary<Type, ComponentStorage> _storage = new Dictionary<Type, ComponentStorage>();
		Dictionary<Type, List<EntityProcessor>> _processorComponentUsage = new Dictionary<Type, List<EntityProcessor>>();
		List<EntityProcessor> _allProcessors = new List<EntityProcessor>();
		Dictionary<ulong, Entity> _allEntities = new Dictionary<ulong, Entity>();
		Dictionary<Type, object> _singletonComponents = new Dictionary<Type, object>();

		IEcsTaskManager _taskManager;
		public int DefragSize = 1000;

		// Entity actions - these are only accessed inside a lock
		object _entitySync = new Object();
		List<DeferredEntityAction> _entityActions = new List<DeferredEntityAction>(1024);
		ulong _giveID = 1;

		// Execution DAG
		Dictionary<Type, ProcessorGraphNode> _nodes = new Dictionary<Type, ProcessorGraphNode>();
		List<ProcessorGraphNode> _sources = new List<ProcessorGraphNode>();
		ManualResetEvent _eventProcessingDone = new ManualResetEvent(false); // TODO: use ManualResetEventSlim instead?
		ManualResetEvent _eventDefragDone = new ManualResetEvent(true); // Needs to be set for the first ecs tick to execute
		int _remainingProcessors = 0;

		// Object pools
		ObjectPool<Entity> _entPool = new ObjectPool<Entity>();
		ObjectPool<DeferredEntityAction> _deferredEntActionPool = new ObjectPool<DeferredEntityAction>();

		public int EntityPoolCount { get { return _entPool.Count; } }
		public int EntityCount { get { return _allEntities.Count; } }

		public EcsManager(IEcsTaskManager taskManager = null)
		{
			if(taskManager == null)
			{
				_taskManager = new EcsTaskManager();
			}
			else
			{
				_taskManager = taskManager;
			}
		}

		void IIndexStorage.OnIndexChanged(ulong owner, Type componentType, int newIndex)
		{
			GetEntityById(owner).ComponentIndices[componentType] = newIndex;
		}

		#region Processor DAG operations

		void OrderProcessors()
		{
			_nodes.Clear();
			_sources.Clear();

			// Create graph nodes
			foreach(var processor in _allProcessors)
			{
				if(_nodes.ContainsKey(processor.GetType()))
				{
					throw new Exception("Multiple processors of same type added: " + processor.GetType().ToString());
				}

				_nodes.Add(processor.GetType(), new ProcessorGraphNode(processor));
			}

			// Build graph
			foreach(var node in _nodes.Values)
			{
				foreach(Type type in node.Processor.ExecuteBefore)
				{
					if(_nodes.ContainsKey(type))
					{
						node.Out.Add(_nodes[type]);
						_nodes[type].In.Add(node);
					}
				}

				foreach(Type type in node.Processor.ExecuteAfter)
				{
					if(_nodes.ContainsKey(type))
					{
						node.In.Add(_nodes[type]);
						_nodes[type].Out.Add(node);
					}
				}
			}

			// Find topologic order and detect any cycles at the same time
			int wave = 1;
			foreach(var node in _nodes.Values)
			{
				Stack<ProcessorGraphNode> stack = new Stack<ProcessorGraphNode>();

				if(!node.HasValidOrder)
				{
					RecurseTopo(node, wave, stack);
					wave++;
				}
			}

			foreach(var node in _nodes.Values)
			{
				if(node.Order == 0)
				{
					_sources.Add(node);
				}
			}

			//_allProcessors = _allProcessors.OrderBy(x => x.TopologicalOrder).ToList();
		}

		void RecurseTopo(ProcessorGraphNode current, int wave, Stack<ProcessorGraphNode> stack)
		{
			int order = 0;

			current.Wave = wave;
			stack.Push(current);

			foreach(var parent in current.In)
			{
				if(!parent.HasValidOrder)
				{
					if(parent.Wave == wave)
					{
						// Cycle detected
						StringBuilder sb = new StringBuilder();

						sb.Append(parent.Processor.GetType().ToString());
						sb.Append(" -> ");
						sb.Append(current.Processor.GetType().ToString());

						do
						{
							stack.Pop();
							sb.Append(" -> ");
							sb.Append(stack.Peek().Processor.GetType().ToString());
						} while(stack.Peek() != parent);

						throw new Exception("Cycle detected in processor dependency graph: " + sb.ToString());
					}

					RecurseTopo(parent, wave, stack);
				}

				order += parent.Order;
			}

			stack.Pop();

			order += current.In.Count;

			current.Order = order;
			current.HasValidOrder = true;
		}

		public string CheckComponentAccess()
		{
			Dictionary<EntityProcessor, HashSet<EntityProcessor>> predecessors = new Dictionary<EntityProcessor, HashSet<EntityProcessor>>();

			foreach(var node in _nodes.Values)
			{
				if(!predecessors.ContainsKey(node.Processor))
				{
					BuildPredecessorsRecursive(predecessors, node);
				}
			}

			Action< Dictionary < Type, HashSet < EntityProcessor >> , Type, EntityProcessor> tryAdd = (dict, type, processor) =>
			{
				if(!dict.ContainsKey(type))
				{
					dict.Add(type, new HashSet<EntityProcessor>());
				}
				dict[type].Add(processor);
			};

			HashSet<Type> componentTypes = new HashSet<Type>();
			// component type -> processor dictionaries
			Dictionary<Type, HashSet<EntityProcessor>> processorsRead = new Dictionary<Type, HashSet<EntityProcessor>>();
			Dictionary<Type, HashSet<EntityProcessor>> processorsWrite = new Dictionary<Type, HashSet<EntityProcessor>>();
			Dictionary<Type, HashSet<EntityProcessor>> processorsUse = new Dictionary<Type, HashSet<EntityProcessor>>();

			// Fill dictionaries
			foreach(var processor in _allProcessors)
			{
				foreach(var componentType in processor.ComponentsRead)
				{
					componentTypes.Add(componentType);
					tryAdd(processorsRead, componentType, processor);
					tryAdd(processorsUse, componentType, processor);
				}

				foreach(var componentType in processor.ComponentsWrite)
				{
					componentTypes.Add(componentType);
					tryAdd(processorsWrite, componentType, processor);
					tryAdd(processorsUse, componentType, processor);
				}
			}

			StringBuilder report = new StringBuilder();

			HashSet<Tuple<EntityProcessor, EntityProcessor>> found = new HashSet<Tuple<EntityProcessor, EntityProcessor>>();

			// Check access
			foreach(var componentType in componentTypes)
			{
				if(!processorsWrite.ContainsKey(componentType))
					continue;

				foreach(var pWrite in processorsWrite[componentType])
				{
					if(!processorsUse.ContainsKey(componentType))
						continue;

					foreach(var pUse in processorsUse[componentType])
					{
						if(pWrite == pUse)
							continue;

						if(!predecessors[pUse].Contains(pWrite) && !predecessors[pWrite].Contains(pUse)
							&& !found.Contains(new Tuple<EntityProcessor, EntityProcessor>(pWrite, pUse))
							&& !found.Contains(new Tuple<EntityProcessor, EntityProcessor>(pUse, pWrite)))
						{
							found.Add(new Tuple<EntityProcessor, EntityProcessor>(pWrite, pUse));
							// The processors do not lie on the same path through the DAG, thus their order is not defined
							report.Append("Undefined order: Component type: ");
							report.Append(componentType.ToString());
							report.Append(" Processor (");

							if(processorsRead[componentType].Contains(pUse))
								report.Append('R');
							if(processorsWrite[componentType].Contains(pUse))
								report.Append('W');

							report.Append("): ");
							report.Append(pWrite.GetType().ToString());
							report.Append(" Processor (");
							if(processorsRead[componentType].Contains(pWrite))
								report.Append('R');
							//if(processorsWrite[componentType].Contains(pWrite))
								report.Append('W');
							report.Append("): ");
							report.Append(pUse.GetType().ToString());
							report.Append(Environment.NewLine);
						}
					}
				}
			}

			return "Found " + found.Count + " problems" + Environment.NewLine + report.ToString();
		}

		void BuildPredecessorsRecursive(Dictionary<EntityProcessor, HashSet<EntityProcessor>> predecessors, ProcessorGraphNode node)
		{
			HashSet<EntityProcessor> localPredecessors = new HashSet<EntityProcessor>();

			foreach(var parent in node.In)
			{
				if(!predecessors.ContainsKey(parent.Processor))
				{
					BuildPredecessorsRecursive(predecessors, parent);
				}

				foreach(var p in predecessors[parent.Processor])
				{
					localPredecessors.Add(p);
				}

				localPredecessors.Add(parent.Processor);
			}

			predecessors.Add(node.Processor, localPredecessors);
		}

		#endregion

		#region Public

		public bool HasProcessor(Type processorType)
		{
			return _nodes.ContainsKey(processorType);
		}

		public void AddProcessors(params EntityProcessor[] processors)
		{
			foreach(var processor in processors)
			{
				processor.SetManager(this);
				processor.GetAttributes();
				processor.ExecuteMethod = GetProcessMethod(processor);

				_allProcessors.Add(processor);

				foreach(Type type in processor.ComponentsRequired)
				{
					if(!_processorComponentUsage.ContainsKey(type))
					{
						_processorComponentUsage.Add(type, new List<EntityProcessor>());
					}

					_processorComponentUsage[type].Add(processor);
				}

				foreach(var component in _singletonComponents)
				{
					processor.TrySetSingletonComponentProperty(component.Value);
				}

				processor.OnInitInternal();
			}

			OrderProcessors();
		}

		public void RemoveProcessors(params EntityProcessor[] processors)
		{
			foreach(var processor in processors)
			{
				_allProcessors.Remove(processor);

				foreach(var entity in processor.AllEntities())
				{
					entity.Processors.Remove(processor);
				}

				processor.OnDestroyInternal();
			}

			OrderProcessors();
		}

		public Entity AddEntity(params object[] components)
		{
			Entity e = _entPool.Get();

			lock (_entitySync)
			{
				e.ID = _giveID;
				e.Alive = true;
				_giveID++;

				var a = _deferredEntActionPool.Get();
				a.Entity = e;
				a.Type = EntityActionType.Create;
				a.Components = components;
				_entityActions.Add(a);
			}

			return e;
		}

		public void RemoveEntity(Entity e)
		{
			lock (_entitySync)
			{
				e.Alive = false;

				var a = _deferredEntActionPool.Get();
				a.Entity = e;
				a.Type = EntityActionType.Destroy;
				_entityActions.Add(a);
			}
		}

		public void EntityAddComponents(Entity e, params object[] components)
		{
			lock (_entitySync)
			{
				var a = _deferredEntActionPool.Get();
				a.Entity = e;
				a.Type = EntityActionType.AddComponents;
				a.Components = components;
				_entityActions.Add(a);
			}
		}

		public void EntityRemoveComponents(Entity e, params Type[] componentTypes)
		{
			lock (_entitySync)
			{
				var a = _deferredEntActionPool.Get();
				a.Entity = e;
				a.Type = EntityActionType.RemoveComponents;
				a.Components = componentTypes;
				_entityActions.Add(a);
			}
		}

		public void SetSingletonProperty(object value)
		{
			if(_singletonComponents.ContainsKey(value.GetType()))
			{
				_singletonComponents[value.GetType()] = value;
			}
			else
			{
				_singletonComponents.Add(value.GetType(), value);
			}

			foreach(var processor in _allProcessors)
			{
				processor.TrySetSingletonComponentProperty(value);
			}
		}

		internal Entity GetEntityById(ulong id)
		{
			return _allEntities[id];
		}

		public string DebugPrintStorage(Type type)
		{
			return _storage[type].PrintContents();
		}

		public void Tick()
		{
			_eventDefragDone.WaitOne();
			_eventDefragDone.Reset();

			_remainingProcessors = _nodes.Count;

			foreach(var node in _nodes.Values)
			{
				node.RemainingDependencies = node.In.Count;
			}

			foreach(var node in _sources)
			{
				_taskManager.EnqueueProcessorTask(GetExecuteProcessorAction(node));
			}

			_eventProcessingDone.WaitOne();
			_eventProcessingDone.Reset();

			Task.Factory.StartNew(() =>
			{
				ProcessDeferredEntityActions();

				foreach(var storage in _storage.Values)
				{
					storage.Defrag();
				}
				foreach(var processor in _allProcessors)
				{
					processor.Defrag();
				}

				_eventDefragDone.Set();
			});
		}

		#endregion

		#region Execution

		Action GetExecuteProcessorAction(ProcessorGraphNode node)
		{
			return () =>
			{
				ExecuteProcessor(node.Processor);

				foreach(var child in node.Out)
				{
					if(Interlocked.Decrement(ref child.RemainingDependencies) == 0)
					{
						_taskManager.EnqueueProcessorTask(GetExecuteProcessorAction(child));
					}
				}

				if(Interlocked.Decrement(ref _remainingProcessors) == 0)
				{
					_eventProcessingDone.Set();
				}
			};
		}

		void ExecuteProcessor(EntityProcessor processor)
		{
			if(processor.ExecutionType == ProcessorExecutionType.DoNotExecure || !processor.AllowExecution)
			{
				processor.OnProcessingStartInternal(0);
				processor.OnProcessingEndInternal();
				return;
			}

			if(processor.ExecutionType == ProcessorExecutionType.Sequential ||
				(processor.ExecutionType == ProcessorExecutionType.PerThreadLoop && processor.ValidCount < processor.MinParallelItems))
			{
				processor.OnProcessingStartInternal(1);
				processor.Execute(0, processor.ContainerSize);
			}

			if(processor.ExecutionType == ProcessorExecutionType.PerThreadLoop && processor.ValidCount >= processor.MinParallelItems)
			{
				_taskManager.ParallelForRange(0, processor.ContainerSize, processor.OnProcessingStartInternal, (start, end) =>
				{
					processor.Execute(start, end - start);
				});
			}

			processor.OnProcessingEndInternal();
		}

		void ProcessDeferredEntityActions()
		{
			lock (_entitySync)
			{
				foreach(var action in _entityActions)
				{
					var e = action.Entity;

					if(action.Type == EntityActionType.Create)
					{
						_allEntities.Add(e.ID, e);

						// Create components
						foreach(var component in action.Components)
						{
							var type = component.GetType();
							var storage = GetStorage(type, true);
							e.ComponentIndices.Add(component.GetType(), storage.Add(e.ID, component));
						}

						var archetype = e.GetArchetype();

						// Add entity to processors
						foreach(var processor in _allProcessors)
						{
							if(Archetype.SystemEntityCompatible(processor.Archetype, archetype))
							{
								processor.AddEnt(e);
								e.Processors.Add(processor);
							}
						}
					}

					if(action.Type == EntityActionType.Destroy)
					{
						_allEntities.Remove(e.ID);

						// Remove components
						foreach(var pair in e.ComponentIndices)
						{
							GetStorage(pair.Key, true).Remove(pair.Value);
						}

						// Remove entity from processors
						foreach(var processor in e.Processors)
						{
							processor.RemoveEnt(e);
						}

						// Return entity back into the object pool (but clean it first)
						e.ID = 0;
						e.Alive = false;
						e.ComponentIndices.Clear();
						e.Processors.Clear();
						_entPool.Return(e);
					}

					if(e.Alive && action.Type == EntityActionType.RemoveComponents)
					{
						// Remove components
						foreach(var component in action.Components)
						{
							var type = (Type)component;

							GetStorage(type, true).Remove(e.ComponentIndices[type]);
							e.ComponentIndices.Remove(type);
						}

						var archetype = e.GetArchetype();

						// Remove entity from processors
						foreach(var processor in e.Processors.ToList())
						{
							if(!Archetype.SystemEntityCompatible(processor.Archetype, archetype))
							{
								processor.RemoveEnt(e);
								e.Processors.Remove(processor);
							}
						}
					}

					if(e.Alive && action.Type == EntityActionType.AddComponents)
					{
						var oldId = e.ID;

						e.ID = _giveID;
						_giveID++;

						// Move old components to new id
						foreach(var pair in e.ComponentIndices.ToList())
						{
							var storage = GetStorage(pair.Key, true);
							int oldIndex = storage.Find(oldId);
							int newIndex = storage.Add(e.ID, storage.Get(oldIndex));
							storage.Remove(oldIndex);
							e.ComponentIndices[pair.Key] = newIndex;
						}

						// Add new components
						foreach(var component in action.Components)
						{
							var type = component.GetType();
							var storage = GetStorage(type, true);
							e.ComponentIndices.Add(component.GetType(), storage.Add(e.ID, component));
						}

						var archetype = e.GetArchetype();

						// Update processors
						foreach(var processor in _allProcessors)
						{
							if(e.Processors.Contains(processor))
							{
								processor.ChangeEntId(oldId, e);
							}
							else
							{
								if(Archetype.SystemEntityCompatible(processor.Archetype, archetype))
								{
									processor.AddEnt(e);
									e.Processors.Add(processor);
								}
							}
						}
					}

					// Clean and return to object pool
					action.Components = null;
					action.Entity = null;
					_deferredEntActionPool.Return(action);
				}

				_entityActions.Clear();
			}
		}

		#endregion

		#region Component storage

		internal ComponentStorage GetStorage(Type componentType, bool allowCreate)
		{
			if(!_storage.ContainsKey(componentType))
			{
				if(!allowCreate)
				{
					throw new Exception("The requested storage type does not exist");
				}

				var storageType = typeof(ComponentStorage<>).MakeGenericType(componentType);
				var obj = Activator.CreateInstance(storageType, this, null);
				var storage = (ComponentStorage)obj;
				_storage.Add(componentType, storage);
				return storage;
			}

			return _storage[componentType];
		}

		internal ComponentStorage<T> GetStorage<T>(bool allowCreate)
		{
			// TODO: or use "as" instead of typecast?
			return (ComponentStorage<T>)GetStorage(typeof(T), allowCreate);
		}

		#endregion

		#region Process method precompilation

		Expression ComponentArg<T>(ParameterExpression indexArray, int i)
		{
			var storage = GetStorage<T>(true);

			return Expression.Property(Expression.Constant(storage), "Item", Expression.ArrayAccess(indexArray, Expression.Constant(i)));
		}

		// Returns a method that takes an array of indices into the processor's component storage and calls the Process() method with correct parameters
		Action<Entity, int[]> GetProcessMethod(EntityProcessor processor)
		{
			if(processor.ExecutionType == ProcessorExecutionType.DoNotExecure)
				return null;

			var entityParameter = Expression.Parameter(typeof(Entity), "entity");
			var arrayParameter = Expression.Parameter(typeof(int[]), "indices");

			List<Expression> arguments = new List<Expression>();
			arguments.Add(entityParameter);
			for(int i = 0; i < processor.ComponentsRequired.Count; i++)
			{
				// Get generic storage access expression by calling ComponentArg
				var argMethod = typeof(EcsManager).GetMethod(nameof(ComponentArg), BindingFlags.NonPublic | BindingFlags.Instance);
				MethodInfo genericMethod = argMethod.MakeGenericMethod(processor.ComponentsRequired[i]);

				// Compile the correct storage access into the method
				arguments.Add((Expression)genericMethod.Invoke(this, new object[]
				{
					arrayParameter, i
				}));
			}

			BlockExpression blockExpr =
				Expression.Block(
					Expression.Call(Expression.Constant(processor), processor.ProcessMethodInfo, arguments)
				);

			return Expression.Lambda<Action<Entity, int[]>>(blockExpr, entityParameter, arrayParameter).Compile();
		}

		#endregion
	}
}
