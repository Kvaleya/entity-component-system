﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Glob;
using Glob.States;
using OpenTK;
using OpenTK.Graphics.OpenGL;

namespace EcsDemo
{
	static class Particle
	{
		public unsafe static void ParticleToIntArray(int* array, int index, Vector2 position, Vector2 velocity, Vector4 color)
		{
			array[index + 0] = Unsafe.SingleToInt32Bits(position.X);
			array[index + 1] = Unsafe.SingleToInt32Bits(position.Y);
			uint vel = 0; // Avoid arithmetic shift
			vel |= (uint)GetVelocityShort(velocity.X);
			vel |= (uint)GetVelocityShort(velocity.Y) << 16;
			array[index + 2] = unchecked((int)vel);
			uint col = 0;
			col |= (uint)GetColorByte(color.X) << 0;
			col |= (uint)GetColorByte(color.Y) << 8;
			col |= (uint)GetColorByte(color.Z) << 16;
			col |= (uint)GetColorByte(color.W) << 24;
			array[index + 3] = unchecked((int)col);
		}

		static ushort GetVelocityShort(float f)
		{
			f = Math.Min(Math.Max(f * 0.5f + 0.5f, 0), 1);

			return (ushort)(ushort.MaxValue * f);
		}

		static byte GetColorByte(float f)
		{
			return (byte)(255 * Math.Min(Math.Max(f, 0f), 1f));
		}
	}

	class Renderer
	{
		const int MaxParticles = 1 << 22;
		const int ParticleIntSize = 4;

		Device Device;
		StreamBuffer _bufferUploadParticles;
		int _bufferParticles;
		int _indexBuffer;
		int _numParticles;
		bool _overflow = false;

		GraphicsPipeline _pso;

		// Fences are inserted at the end of the frame
		int _maxFences = 16;
		// Keep the fences of last several frames
		Queue<FenceSync> _fencesFrameEnd = new Queue<FenceSync>();

		public Renderer(Device device)
		{
			Device = device;

			const int len = MaxParticles * ParticleIntSize * 4;
			_bufferUploadParticles = new StreamBuffer("BufferUploadParticles", BufferTarget.CopyReadBuffer, len);
			_bufferParticles = Utils.CreateBuffer(BufferTarget.ShaderStorageBuffer, new IntPtr(len), IntPtr.Zero, BufferStorageFlags.None,
				"BufferParticles");

			// Create index buffer of two triangle quads
			unsafe
			{
				var ints = new int[MaxParticles * 6];

				fixed (int* array = ints)
				{
					for(int i = 0; i < MaxParticles; i++)
					{
						array[i * 6 + 0] = i * 4 + 0;
						array[i * 6 + 1] = i * 4 + 1;
						array[i * 6 + 2] = i * 4 + 2;
						array[i * 6 + 3] = i * 4 + 2;
						array[i * 6 + 4] = i * 4 + 1;
						array[i * 6 + 5] = i * 4 + 3;
					}

					_indexBuffer = Utils.CreateBuffer(BufferTarget.ElementArrayBuffer, new IntPtr(MaxParticles * 6 * 4), new IntPtr(array), 
						BufferStorageFlags.None, "BufferParticlesIndices");
				}
			}

			_pso = new GraphicsPipeline(Device, Device.GetShader("particle.vert"), Device.GetShader("particle.frag"), null, new RasterizerState(CullfaceState.None), new DepthState(DepthFunction.Always, false), new BlendState(BlendMode.Additive));
		}

		public unsafe void AddParticle(Vector2 position, Vector2 velocity, Vector4 color)
		{
			int index = Interlocked.Increment(ref _numParticles) - 1;

			if(index >= MaxParticles)
			{
				if(!_overflow)
				{
					_overflow = true;
					Device.TextOutput.Print(OutputTypeGlob.Warning, "Particle buffer overflow!");
				}
				return;
			}

			Particle.ParticleToIntArray((int*)_bufferUploadParticles.CurrentStart, index * 4, position, velocity, color);
		}

		public void Render(int w, int h)
		{
			Device.Invalidate();
			FenceSync frameSync = new FenceSync(0);

			// Copy particles to GPU memory
			int copyOffset = _bufferUploadParticles.CurrentOffset;
			_bufferUploadParticles.Advance(frameSync);
			GL.MemoryBarrier(MemoryBarrierFlags.ClientMappedBufferBarrierBit);
			Utils.CopyBuffer(_bufferUploadParticles.Handle, _bufferParticles, copyOffset, 0, _numParticles * ParticleIntSize * 4);

			// Setup viewport
			GL.Viewport(0, 0, w, h);
			GL.ClearColor(0, 0, 0, 0);
			GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

			// Render
			GL.BindBuffer(BufferTarget.ElementArrayBuffer, _indexBuffer);
			Device.BindPipeline(_pso);
			Device.BindBufferBase(0, BufferRangeTarget.ShaderStorageBuffer, _bufferParticles);
			Device.ShaderVertex.SetUniformF("aspect", (float)w / h);
			GL.DrawElements(PrimitiveType.Triangles, _numParticles * 6, DrawElementsType.UnsignedInt, IntPtr.Zero);

			_numParticles = 0;
			GL.MemoryBarrier(MemoryBarrierFlags.ClientMappedBufferBarrierBit);
			frameSync.Create();

			_fencesFrameEnd.Enqueue(frameSync);
			if(_fencesFrameEnd.Count > _maxFences)
			{
				var fenceDelete = _fencesFrameEnd.Dequeue();
				fenceDelete.Dispose();
			}
		}
	}

	static unsafe class Unsafe
	{
		// https://stackoverflow.com/questions/20981551/c-sharp-marshal-copy-intptr-to-16-bit-managed-unsigned-integer-array
		public static void Copy(int[] source, IntPtr ptrDest, int start, uint elements)
		{
			fixed (int* ptrSource = &source[start])
			{
				CopyMemory(ptrDest, (IntPtr)ptrSource, elements * 8);
			}
		}

		public static int SingleToInt32Bits(float value)
		{
			return *(int*)(&value);
		}
		public static float Int32BitsToSingle(int value)
		{
			return *(float*)(&value);
		}

		[DllImport("kernel32.dll", EntryPoint = "RtlMoveMemory", SetLastError = false)]
		static extern void CopyMemory(IntPtr destination, IntPtr source, uint length);
	}
}
