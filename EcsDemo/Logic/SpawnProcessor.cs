﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EcsLib;
using OpenTK;

namespace EcsDemo.Logic
{
	[ProcessorExecution(ProcessorExecutionType.PerThreadLoop)]
	class SpawnProcessor : EntityProcessor
	{
		[SingletonComponent]
		public SimulationContext Context { get; set; }

		Random _random = new Random();

		const int SpawnRate = 10000;
		const int MaxEnts = 50000;
		long _spawnTicks;
		long _bufferTicks = 0;
		const double TimeToLive = 5;

		int _entCount = 0;

		public SpawnProcessor()
		{
			_spawnTicks = (Stopwatch.Frequency / SpawnRate);
		}

		protected override void OnProcessingStart(int numThreads)
		{
			base.OnProcessingStart(numThreads);

			_bufferTicks += Context.DeltaTicks;

			int toSpawn = (int)(_bufferTicks / _spawnTicks);
			_bufferTicks -= toSpawn * _spawnTicks;

			toSpawn = Math.Max(Math.Min(toSpawn, MaxEnts - _entCount), 0);

			for(int i = 0; i < toSpawn; i++)
			{
				SpawnParticle();
			}
		}

		void SpawnParticle()
		{
			Interlocked.Increment(ref _entCount);

			var color = _random.NextVector3();
			color /= Math.Max(Math.Max(color.X, color.Y), color.Z);
			color *= 0.1f;

			Manager.AddEntity(new TransformComponent()
			{
				Position = new Vector2(0, 0.25f),
				Velocity = (_random.NextVector2() * 2.0f - Vector2.One) * 2f,
			}, new ColorComponent()
			{
				Color = new Vector4(color, 0),
			}, new LifetimeComponent()
			{
				Created = Context.Ellapsed,
				TimeToLive = TimeToLive,
			}, new FadeInComponent());
		}

		public void Process(Entity e, ref LifetimeComponent lifetime)
		{
			if(lifetime.Created + lifetime.TimeToLive < Context.Ellapsed)
			{
				Interlocked.Decrement(ref _entCount);
				Manager.RemoveEntity(e);
			}
		}
	}
}
