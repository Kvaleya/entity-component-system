﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcsLib;
using OpenTK;

namespace EcsDemo.Logic
{
	[ProcessorExecuteBefore(typeof(RenderProcessor))]
	[ProcessorExecution(ProcessorExecutionType.PerThreadLoop)]
	class FadeInProcessor : EntityProcessor
	{
		[SingletonComponent]
		public SimulationContext Context { get; set; }

		public FadeInProcessor()
		{
		}

		public void Process(Entity e, [ComponentReadOnly] ref FadeInComponent fadeIn, ref ColorComponent color)
		{
			color.Color.W += (float)Context.DeltaT * 1.5f;
			if(color.Color.W >= 1)
			{
				color.Color.W = 1;
				Manager.EntityRemoveComponents(e, typeof(FadeInComponent));
			}
		}
	}
}
