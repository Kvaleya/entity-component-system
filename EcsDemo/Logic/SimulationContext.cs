﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcsDemo.Logic
{
	class SimulationContext
	{
		public double DeltaT = 0;
		public double Ellapsed = 0;
		public long EllapsedTicks;
		public long DeltaTicks;
		public int ScreenWidth;
		public int ScreenHeight;
	}
}
