﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EcsDemo.Logic
{
	struct LifetimeComponent
	{
		public double Created;
		public double TimeToLive;
	}
}
