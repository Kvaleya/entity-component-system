﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcsLib;
using OpenTK;

namespace EcsDemo.Logic
{
	[ProcessorExecution(ProcessorExecutionType.PerThreadLoop)]
	class MovementProcessor : EntityProcessor
	{
		[SingletonComponent]
		public SimulationContext Context { get; set; }

		public void Process(Entity e, ref TransformComponent transform)
		{
			transform.Velocity += new Vector2(0, -1) * (float)Context.DeltaT;
			transform.Position += transform.Velocity * (float)Context.DeltaT * 0.1f;
		}
	}
}
