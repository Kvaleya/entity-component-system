﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EcsDemo.Logic
{
	struct TransformComponent
	{
		public Vector2 Position;
		public Vector2 Velocity;
	}
}
