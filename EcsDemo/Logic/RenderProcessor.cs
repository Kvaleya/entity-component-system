﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcsLib;
using OpenTK;

namespace EcsDemo.Logic
{
	[ProcessorExecuteAfter(typeof(MovementProcessor))]
	[ProcessorExecution(ProcessorExecutionType.PerThreadLoop)]
	class RenderProcessor : EntityProcessor
	{
		[SingletonComponent]
		public SimulationContext Context { get; set; }

		Renderer _renderer;

		public RenderProcessor(Renderer renderer)
		{
			_renderer = renderer;
		}

		public void Process(Entity e, [ComponentReadOnly] ref TransformComponent transform, [ComponentReadOnly] ref ColorComponent color)
		{
			float aspect = Context.ScreenWidth / (float)Context.ScreenHeight;
			Vector2 p = transform.Position;
			p.Y *= aspect;

			const float border = 1.1f;

			if(Math.Abs(p.X) < border && Math.Abs(p.Y) < border)
				_renderer.AddParticle(transform.Position, transform.Velocity, color.Color);
		}
	}
}
