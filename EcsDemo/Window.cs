﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EcsDemo.Logic;
using EcsLib;
using GameFramework;
using Glob;
using OpenTK;

namespace EcsDemo
{
	class Window : GameWindow
	{
		const string TitleString = "ECS Demo; FPS: {0}; EntityCount: {1}; EcsTime: {2} ms; EntPool: {3}";

		Device _device;
		TextOutput _textOutput;
		IFileManager _fileManager;
		Stopwatch _sw;
		Stopwatch _swFramerate;
		Stopwatch _swEcs;
		int fpsFrameCounter = 0;
		long _lastTicks = 0;

		Renderer _renderer;

		EcsManager _ecsManager;
		SimulationContext _simulationContext;

		public Window(int w, int h)
			: base(w, h)
		{
		}

		protected override void OnLoad(EventArgs e)
		{
			base.OnLoad(e);
			_textOutput = TextOutput.CreateCommon();
			_fileManager = new FileManagerZip(_textOutput, "");

			var textOutputGlob = new TextOutputGlob(_textOutput);

			_device = new Device(textOutputGlob, new FileManagerGlob(_fileManager, "Shaders", "Shaders/Resolved", "Shaders/Compiled"));

			DebugMessageManager.SetupDebugCallback(textOutputGlob);

			this.VSync = VSyncMode.Off;

			_renderer = new Renderer(_device);

			_ecsManager = new EcsManager();
			_simulationContext = new SimulationContext();
			_ecsManager.SetSingletonProperty(_simulationContext);

			//_ecsManager.AddProcessors(new RenderProcessor(_renderer), new MovementProcessor(), new SpawnProcessor());
			_ecsManager.AddProcessors(new RenderProcessor(_renderer), new MovementProcessor(), new SpawnProcessor(), new FadeInProcessor());
			_ecsManager.DefragSize = 30000;

			_textOutput.Print(_ecsManager.CheckComponentAccess());

			_sw = new Stopwatch();
			_sw.Start();
			_swFramerate = new Stopwatch();
			_swFramerate.Start();
			_swEcs = new Stopwatch();
		}

		protected override void OnRenderFrame(FrameEventArgs e)
		{
			base.OnRenderFrame(e);
			_device.Update();

			_simulationContext.EllapsedTicks = _sw.ElapsedTicks;
			_simulationContext.DeltaTicks = _simulationContext.EllapsedTicks - _lastTicks;
			_lastTicks = _simulationContext.EllapsedTicks;
			_simulationContext.DeltaT = _simulationContext.DeltaTicks / (double)Stopwatch.Frequency;
			_simulationContext.Ellapsed = _simulationContext.EllapsedTicks / (double)Stopwatch.Frequency;

			_simulationContext.ScreenWidth = Width;
			_simulationContext.ScreenHeight = Height;

			_swEcs.Restart();
			_ecsManager.Tick();
			_swEcs.Stop();

			_renderer.Render(Width, Height);

			SwapBuffers();
			fpsFrameCounter++;

			// FPS counter
			if(_swFramerate.ElapsedTicks > Stopwatch.Frequency)
			{
				double fps = fpsFrameCounter / (_swFramerate.ElapsedTicks / (double)Stopwatch.Frequency);
				_swFramerate.Restart();
				fpsFrameCounter = 0;

				this.Title = String.Format(TitleString, fps.ToString("0.##", CultureInfo.InvariantCulture), _ecsManager.EntityCount, GetMs(_swEcs.ElapsedTicks), _ecsManager.EntityPoolCount);
			}
		}

		string GetMs(long tick)
		{
			return (tick / (double)Stopwatch.Frequency * 1000).ToString("0.00");
		}
	}
}
