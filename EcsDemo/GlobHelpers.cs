﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameFramework;
using Glob;

namespace EcsDemo
{
	// Helper classes for using Glob library

	// Can be passed to GlobContext
	class TextOutputGlob : ITextOutputGlob
	{
		TextOutput _textOutput;

		public TextOutputGlob(TextOutput textOutput)
		{
			_textOutput = textOutput;
		}

		public void Print(OutputTypeGlob type, string message)
		{
			OutputType ot = OutputType.Notify;

			if(type == OutputTypeGlob.LogOnly)
				ot = OutputType.LogOnly;
			if(type == OutputTypeGlob.Debug)
				ot = OutputType.Debug;
			if(type == OutputTypeGlob.Notify)
				ot = OutputType.Notify;
			if(type == OutputTypeGlob.Warning)
				ot = OutputType.Warning;
			if(type == OutputTypeGlob.PerformanceWarning)
				ot = OutputType.PerformanceWarning;
			if(type == OutputTypeGlob.Error)
				ot = OutputType.Error;

			_textOutput.Print(message, ot);
		}
	}

	// Can be passed to GlobContext
	class FileManagerGlob : IFileManager, Glob.IFileManagerGlob
	{
		IFileManager _fileManager;

		string _shaderSourcePath;
		string _shaderResolvedPath;
		string _shaderCompilePath;

		public FileManagerGlob(IFileManager fileManager, string shaderSourcePath, string shaderResolvedPath, string shaderCompiledPath)
		{
			_fileManager = fileManager;
			_shaderSourcePath = shaderSourcePath;
			_shaderResolvedPath = shaderResolvedPath;
			_shaderCompilePath = shaderCompiledPath;
		}

		public string GetPathShaderSource(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderSourcePath;
			return Path.Combine(_shaderSourcePath, file);
		}

		public string GetPathShaderCacheResolved(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderResolvedPath;
			return Path.Combine(_shaderResolvedPath, file);
		}

		public string GetPathShaderCacheCompiled(string file)
		{
			if(string.IsNullOrEmpty(file))
				return _shaderCompilePath;
			return Path.Combine(_shaderCompilePath, file);
		}

		public void MountArchive(string archive)
		{
			_fileManager.MountArchive(archive);
		}

		public bool FileExists(string file)
		{
			return _fileManager.FileExists(file);
		}

		Stream IFileManager.GetStream(string file)
		{
			return _fileManager.GetStream(file);
		}

		Stream IFileManagerGlob.GetStream(string file)
		{
			return _fileManager.GetStream(file);
		}
	}

}
