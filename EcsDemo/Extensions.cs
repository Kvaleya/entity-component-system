﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OpenTK;

namespace EcsDemo
{
	static class Extensions
	{
		public static float NextFloat(this Random random)
		{
			return (float)random.NextDouble();
		}

		public static Vector2 NextVector2(this Random random)
		{
			return new Vector2((float)random.NextDouble(), (float)random.NextDouble());
		}

		public static Vector3 NextVector3(this Random random)
		{
			return new Vector3((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
		}

		public static Vector4 NextVector4(this Random random)
		{
			return new Vector4((float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble(), (float)random.NextDouble());
		}
	}
}
