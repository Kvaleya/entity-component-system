#version 430

out gl_PerVertex {
    vec4 gl_Position;
};

layout(location = 0) out vec2 vCoord;
layout(location = 1) out vec4 vColor;

layout(binding = 0, std430) restrict readonly buffer bufferParticles
{
    uvec4 particles[];
};

uniform float aspect;

void UnpackParticle(int i, out vec2 pos, out vec2 vel, out vec4 col)
{
	uvec4 p = particles[i];
	pos.x = uintBitsToFloat(p.x);
	pos.y = uintBitsToFloat(p.y);
	vel.x = (float(p.z & 0xffff) / 65535.0) * 2.0 - 1.0;
	vel.y = (float((p.z >> 16) & 0xffff) / 65535.0) * 2.0 - 1.0;
	col.x = float((p.w >> 0) & 0xff) / 255.0;
	col.y = float((p.w >> 8) & 0xff) / 255.0;
	col.z = float((p.w >> 16) & 0xff) / 255.0;
	col.w = float((p.w >> 24) & 0xff) / 255.0;
}

vec2 positions[4] = vec2[4](
	vec2(-1.0, -1.0),
	vec2(1.0, -1.0),
	vec2(-1.0, 1.0),
	vec2(1.0, 1.0)
);

void main() {
	vec2 pos;
	vec2 vel;
	
	UnpackParticle(gl_VertexID / 4, pos, vel, vColor);
	
	vCoord = positions[gl_VertexID & 0x3];
	
	vec2 forward = vec2(1.0, 0.0);
	float velLength = length(vel);
	if(velLength > 0)
	{
		forward = vel / velLength;
	}
	vec2 right = vec2(forward.y, -forward.x);
	forward *= 1.0 + clamp(velLength * 3,0 , 3);
	
	vec2 vertex = pos + (vCoord.x * forward + vCoord.y * right) * 0.005;
	
	gl_Position = vec4(vertex.x, vertex.y * aspect, 0.0, 1.0);
}