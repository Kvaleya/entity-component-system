#version 430

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec2 vCoord;
layout(location = 1) in vec4 vColor;

void main() {
	float circle = clamp(1.0 - dot(vCoord, vCoord), 0.0, 1.0);
	outColor = vec4(circle * vColor.w * vColor.xyz, 1.0);
}